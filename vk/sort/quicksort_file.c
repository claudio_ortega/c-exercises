#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void cancel( const char* s ) {
    perror(s);
    exit(EXIT_FAILURE);
}

int compare (const void *a, const void *b) {
    char* stringA = * (char**) a;
    char* stringB = * (char**) b;
    if ( strcmp(stringA, stringB ) > 0 ) {
        return 1;
    }
    if ( strcmp(stringA, stringB ) < 0 ) {
        return -1;
    }
    return 0;
}

int main ()
{
    const int BUFF_LEN = 255;
    char buffer[BUFF_LEN] = {0};

    char *result = getcwd(buffer, BUFF_LEN);
    printf("result:[%s]\n", result);

    FILE* filePointer = fopen("file.txt", "r");
    if ( filePointer == NULL ) {
        cancel("fopen failed\n");
    }

    int lineCount = 0;
    while (fgets(buffer, BUFF_LEN, filePointer)) {
        lineCount++;
    }
    fclose(filePointer);

    filePointer = fopen("file.txt", "r");
    if ( filePointer == NULL ) {
        cancel("fopen failed\n");
    }

    char* *array = malloc( sizeof(char*) * lineCount );
    for (int j=0; j < lineCount; j++) {
        fgets(buffer, BUFF_LEN, filePointer);
        array[j] = malloc(strlen(buffer));
        strcpy (array[j], buffer );
    }
    fclose(filePointer);

    for ( int j=0; j<lineCount; j++) {
        printf("(1) %04d |%s", j, array[j]);
    }

    qsort ( array, lineCount, sizeof(array[0]), compare );

    for ( int j=0; j<lineCount; j++) {
        printf("(2) %04d |%s", j, array[j]);
    }

    for ( int j=0; j<lineCount; j++) {
        free (array[j]);
    }

    free(array);

    return 0;
}

