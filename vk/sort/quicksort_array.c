#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare (const void *a, const void *b) {

    char* stringA = * (char**) a;
    char* stringB = * (char**) b;

    if ( strcmp(stringA, stringB ) > 0 ) {
        return 1;
    }

    if ( strcmp(stringA, stringB ) < 0 ) {
        return -1;
    }

    return 0;
}

int main ()
{
    char* array[] = {"z", "a", "k" };
    int len = sizeof(array) / sizeof(array[0]);

    for ( int j=0; j<len; j++) {
        printf("(1) %04d |%s\n", j, array[j]);
    }

    qsort ( array, len, sizeof(array[0]), compare );

    for ( int j=0; j<len; j++) {
        printf("(2) %04d |%s\n", j, array[j]);
    }

    return 0;
}

