#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>

char* formatTime() {
    time_t current_time = time(NULL);
    struct tm *tm = localtime(&current_time);
    static char buf[100] = {0};
    sprintf( buf, "%02d:%02d:%02d", tm->tm_hour, tm->tm_min, tm->tm_sec );
    return buf;
}

typedef struct ThreadData {
    char* threadName;
    sem_t* mutex;
} ThreadData;

void* task(void* arg)
{
    ThreadData* tData = (ThreadData*)arg;
    sem_wait(tData->mutex);
    printf("%s -- entered critical section on %s\n", formatTime(), tData->threadName);
    sleep(4);
    sem_post(tData->mutex);
    printf("%s -- exited critical section on %s\n", formatTime(), tData->threadName);
    return NULL;
}

int main()
{
    printf("%s -- mutex exercise\n", formatTime());

    sem_t* mutexA = sem_open("/s", O_CREAT, 0600, 1);

    pthread_t t1 = {0};
    ThreadData tdata1 = {0};
    tdata1.threadName = "thread1";
    tdata1.mutex = mutexA;
    pthread_create(&t1, NULL, task, &tdata1);

    printf("%s -- before sleep\n", formatTime());
    sleep(2);
    printf("%s -- after sleep\n", formatTime());

    pthread_t t2 = {0};
    ThreadData tdata2 = {0};
    tdata2.threadName = "thread2";
    tdata2.mutex = mutexA;
    pthread_create(&t2, NULL, task, &tdata2);

    printf("%s -- before join\n", formatTime());
    pthread_join(t1,NULL);
    pthread_join(t2,NULL);
    printf("%s -- after join\n", formatTime());

    sem_close(mutexA);
    printf("%s -- done\n", formatTime());

    return 0;
}

