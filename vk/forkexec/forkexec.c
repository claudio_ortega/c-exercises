#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void cancel( const char* s ) {
    perror(s);
    exit(EXIT_FAILURE);
}

int main ()
{
    pid_t childPID = fork();

    if ( childPID < 0 ) {
        cancel( "fork() failed\n" );
    }

    if ( childPID == 0 ) {
        printf("this is the child, so we will replace this process space with another one\n");
        char* const argvChild[] = {"", "-l", NULL};
        int childStatus = execv("/bin/ls", argvChild);
        if ( childStatus < 0 ) {
            printf("execv failed with childStatus:%d\n", childStatus);
        }
        else {
            printf("in case of success the process gets swapped and so this line cannot ever possibly execute\n" );
        }
    }

    if ( childPID > 0 ) {
        printf("this is the parent, the child pid is [%d]\n", childPID);
    }

    return 0;
}

