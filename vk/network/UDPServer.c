#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

void cancel( const char* s ) {
    perror(s);
    exit(EXIT_FAILURE);
}

int main() {
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if ( sockfd < 0 ) {
        cancel("socket creation failed");
    }

    struct sockaddr_in servaddr = {0};
    struct sockaddr_in cliaddr = {0};
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(5555);

    int result = bind(
        sockfd,
        (const struct sockaddr *)&servaddr,
        sizeof(servaddr));
    if ( result < 0 )
    {
        cancel("bind failed");
    }

    socklen_t len = sizeof(cliaddr);
    const int MAXLINE = 1024;
    char buffer[MAXLINE+1] = {0};

    const int MAX_LOOPS = 10;
    for ( int i=0;i<10;i++) {
        memset(buffer, 0, sizeof (buffer));

        printf("waiting to receive from client, loop: [%d/%d]\n", i, MAX_LOOPS);
        recvfrom(
            sockfd,
            (char *)buffer,
            MAXLINE,
            MSG_WAITALL,
            ( struct sockaddr *) &cliaddr,
            &len);
        printf("received from client:[%s]\n", buffer);

        memset(buffer, 0, sizeof (buffer));
        snprintf(buffer, MAXLINE, "hello ack from server, loop:[%d/%d]", i, MAX_LOOPS );
        sendto(
            sockfd,
            (const char *)buffer,
            strlen(buffer),
            0,
            (const struct sockaddr *) &cliaddr,
            len);

        printf("hello ack msg sent:[%s]\n", buffer);
    }

    return 0;
}
