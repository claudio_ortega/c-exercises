#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

void cancel( const char* s ) {
    perror(s);
    exit(EXIT_FAILURE);
}

int main() {
    printf("server starts\n");

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if ( sockfd < 0 ) {
        cancel("socket creation failed\n");
    }
    printf("socket OK\n");

    struct sockaddr_in servaddr = {0};
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(5555);

    int result = bind(
        sockfd,
        (const struct sockaddr *)&servaddr,
        sizeof(servaddr));
    if ( result < 0 )
    {
        cancel("bind failed\n");
    }
    printf("bind OK\n");

    result = listen(sockfd, 5);
    if ( result < 0 )
    {
        cancel("listen failed\n");
    }

    printf("listen OK\n");

    struct sockaddr_in clientaddr = {0};
    socklen_t client_address_len = sizeof(clientaddr);

    printf("waiting for a client\n");
    int clientSockFd = accept(sockfd, (struct sockaddr *)&clientaddr, &client_address_len);

    if ( clientSockFd < 0 )
    {
        cancel("accept failed\n");
    }

    printf("accept OK\n");

    const int MAXLINE = 1024;
    char buffer[MAXLINE] = {0};
    const char* msg = "msg back from server";

    while(1) {
        memset(buffer, 0, sizeof (buffer));

        printf("waiting to receive from client\n");
        ssize_t received_len = read(
            clientSockFd,
            (char *)buffer,
            sizeof(buffer) );

        if ( received_len < 0 ) {
            cancel("error on read()\n");
        }

        if ( received_len == 0 ) {
            printf("client closed the socket()\n");
            break;
        }

        printf("received from client: %ld, [%s]\n", received_len, buffer);

        strcpy( buffer, msg );
        write(clientSockFd, buffer, sizeof(buffer));

        printf("sent to client:[%s]\n", buffer);
    }

    close(sockfd);

    printf("done\n");

    return 0;
}
