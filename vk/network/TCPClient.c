#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>


void cancel( const char* s ) {
    perror(s);
    exit(EXIT_FAILURE);
}

int main() {

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if ( sockfd < 0 ) {
        cancel("socket creation failed\n");
    }

    struct sockaddr_in servaddr = {0};

    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(5555);
    servaddr.sin_addr.s_addr = htonl( INADDR_ANY );

    int status = connect(
        sockfd,
        (const struct sockaddr *) &servaddr,
        sizeof(servaddr));

    if ( status < 0 ) {
        cancel("connection failed\n");
    }

    char *hello = "Hello from client";
    const int MAXLINE = 1024;
    char buffer[MAXLINE] = {0};
    strcpy( buffer, hello);

    int n = write(
        sockfd,
        (char *)buffer,
        sizeof (buffer));
    printf("write returned length: %d\n", n);

    memset(buffer, 0, sizeof(buffer));
    read(sockfd, buffer, sizeof(buffer));

    printf("received from server: [%s]\n", buffer);

    close(sockfd);

    printf("done\n");

    return 0;
}
