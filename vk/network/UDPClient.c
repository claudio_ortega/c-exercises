#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

void cancel( const char* s ) {
    perror(s);
    exit(EXIT_FAILURE);
}

int main() {

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if ( sockfd < 0 ) {
        cancel("socket creation failed");
    }

    struct sockaddr_in servaddr = {0};

    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(5555);
    servaddr.sin_addr.s_addr = INADDR_ANY;

    char *hello = "Hello from client";
    sendto(
        sockfd,
        (const char *)hello,
        strlen(hello),
        0,
        (const struct sockaddr *) &servaddr,
        sizeof(servaddr));
    printf("hello msg was sent.\n");

    const int MAXLINE = 1024;
    char buffer[MAXLINE+1] = {0};
    socklen_t len = 0;
    int n = recvfrom(
        sockfd,
        (char *)buffer, MAXLINE,
        MSG_WAITALL,
        (struct sockaddr *) &servaddr,
        &len);
    close(sockfd);

    buffer[n] = '\0';
    printf("msg received from server: [%s]\n", buffer);

    return 0;
}
