#include <stdlib.h>
#include <printf.h>
#include <string.h>

#include "lru_list.h"
#include "lru_cache.h"

LRUCache* createCache(int capacity) {
    LRUCache* newCache = (LRUCache*) malloc(sizeof(LRUCache));
    newCache -> capacity = capacity;
    newCache -> arrayOfLists = (List**) malloc(sizeof(List*) * capacity);
    return newCache;
}

void printCache(LRUCache* cache) {
    for ( int i=0; i<cache->capacity; i++ ) {
        List* list = cache->arrayOfLists[i];
        printf( "\nslot: %d, %p\n", i, list );
        if ( list == NULL ) {
            printf("null slot");
        }
        else {
            printList( list );
        }
    }
}

unsigned int computeHash ( char* key, int modulus ) {
    unsigned int hash = 0;
    for ( int i=0; i<strlen(key); i++ ) {
        hash += ( hash * 31 + key[i] );
    }
    return hash % modulus;
}

int put2(LRUCache* cache, char* key, char* value) {
    return put1(cache, key, value, strlen(value) );
}

int put1(LRUCache* cache, char* key, void* value, int len) {
    unsigned int slot = computeHash(key, cache->capacity );

    List* list = cache->arrayOfLists[slot];
    if ( list == NULL ) {
        list = createList(cache->capacity );
        cache->arrayOfLists[slot] = list;
    }

    Node* node = createNode1( key, value, len);
    int status = addHeadSide( list, node );

    return status;
}

Node* get(LRUCache* cache, char* key) {
    unsigned int slot = computeHash(key, cache->capacity );
    List* list = cache->arrayOfLists[slot];
    if ( list == NULL ) {
        return NULL;
    }
    Node* node = findByKey(list, key);
    moveToHead(list, key);
    return node;
}
