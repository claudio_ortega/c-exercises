#ifndef LRULIST_H_
#define LRULIST_H_

typedef struct Node {
    char* key;
    char* value;
    int len;
    struct Node* next;
    struct Node* prev;
} Node;

typedef struct {
    int capacity;
    int length;
    Node *head;
    Node *tail;
} List;

Node* createNode1(char* key, void* value, int len);
Node* createNode2(char* key, char* value);
List* createList(int capacity);
int addHeadSide(List*, Node*);
int removeTailSide(List*);
Node* findByKey(List*, char* key);
void moveToHead(List*, char* key);
void printList(List*);
void printNode(Node* node);

#endif // LRULIST_H_
