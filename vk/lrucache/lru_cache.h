#ifndef LRUCACHE_H_
#define LRUCACHE_H_

#include "lru_list.h"

typedef struct {
    int capacity;
    List* *arrayOfLists;
} LRUCache;

LRUCache* createCache(int size);
void printCache(LRUCache* cache);
int put1(LRUCache*, char* key, void* value, int len);
int put2(LRUCache*, char* key, char* value);
Node* get(LRUCache*, char* key);

#endif // LRUCACHE_H_
