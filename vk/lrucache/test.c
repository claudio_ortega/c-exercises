#include "test.h"
#include "lru_cache.h"
#include <stdio.h>

int test1() {
    printf( "test1() -- BEGIN\n" );

    Node* n1 = createNode2("key1", "value1");
    Node* n2 = createNode2("key2", "value2");
    Node* n3 = createNode2("key3", "value3");

    List* list = createList(3);
    printList( list );

    addHeadSide(list, n1);
    printList( list );

    addHeadSide(list, n2);
    printList( list );

    addHeadSide(list, n3);
    printList( list );

    removeTailSide(list);
    printList( list );

    removeTailSide(list);
    printList( list );

    removeTailSide(list);
    printList( list );

    printf( "test1() -- END\n" );
    return 0;
}

int test2() {
    printf( "test2() -- BEGIN\n" );

    Node* n1 = createNode2("key1", "value1");
    Node* n2 = createNode2("key2", "value2");
    Node* n3 = createNode2("key3", "value3");

    List* list = createList(3);
    addHeadSide(list, n3);
    addHeadSide(list, n2);
    addHeadSide(list, n1);
    printList( list );

    removeTailSide( list );
    printList( list );

    removeTailSide( list );
    printList( list );

    removeTailSide( list );
    printList( list );

    printf( "test2() -- END\n" );
    return 0;
}

int test3() {
    printf( "test3() -- BEGIN\n" );

    Node* n1 = createNode2("key1", "value1");
    Node* n2 = createNode2("key2", "value2");
    Node* n3 = createNode2("key3", "value3");

    List* list = createList(3);
    addHeadSide(list, n3);
    addHeadSide(list, n2);
    addHeadSide(list, n1);
    printList( list );

    moveToHead(list, "key3" );
    printList( list );

    printf( "test3() -- END\n" );
    return 0;
}

int test4() {

    printf( "test4() -- BEGIN\n" );

    LRUCache* cache = createCache(3);
    printCache(cache);

    put2(cache, "key1", "value1");
    put2(cache, "key2", "value2");
    put2(cache, "key3", "value3");

    printCache(cache);

    Node* r1 = get(cache, "key1");
    Node* r2 = get(cache, "key2");
    Node* r3 = get(cache, "key3");

    printNode ( r1 );
    printNode ( r2 );
    printNode ( r3 );

    printf( "test4() -- END\n" );

    return 0;
}


