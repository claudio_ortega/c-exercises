#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "lru_list.h"

Node* createNode2(char* key, char* value) {
    return createNode1( key, value, strlen(value) );
}

Node* createNode1(char* key, void* value, int len) {
    Node* new = (Node*) malloc( sizeof(Node) );
    new->key = key;
    new->value = value;
    new->len = len;
    new->next = NULL;
    new->prev = NULL;
    return new;
}

List* createList(int capacity) {
    List* new = (List*)malloc( sizeof(List) );
    new->capacity = capacity;
    new->length = 0;
    new->head = NULL;
    new->tail = NULL;
    return new;
}

void addFirst(List* list, Node* node) {
    assert( list->length == 0 );
    assert( list->capacity > 0 );
    list->length = 1;
    list->head = node;
    list->tail = node;
    node->prev = NULL;
    node->next = NULL;
}

int addHeadSide(List* list, Node* node) {
    if ( list->length == list->capacity ) {
        return 1;
    }
    if ( list->length == 0 ) {
        addFirst(list, node);
        return 0;
    }
    list->length++;
    node->prev = NULL;
    list->head->prev = node;
    node->next = list->head;
    list->head = node;
    return 0;
}

Node* findByKey(List* list, char* key) {
    Node* current = list->head;
    while ( current != NULL ) {
        if ( strcmp(key, current->key) == 0 ) {
            return current;
        }
        current = current->next;
    }
    return NULL;
}

void removeNode(List* list, Node* node) {

    assert ( list->length > 0 );

    if ( list->length == 1 ) {
        list->tail = NULL;
        list->head = NULL;
        list->length = 0;
        return;
    }

    // node is the tail node,
    // so after removal node->prev becomes tail
    if ( node->next == NULL ) {
        list->tail = node->prev;
        node->prev->next = NULL;
    }

    // node is the head node,
    // so after removal node->next becomes head
    else if ( node->prev == NULL ) {
        list->head = node->next;
        node->next->prev = NULL;
    }

    // somewhere in the middle
    else {
        node->next->prev = node->prev;
        node->prev->next = node->next;
    }

    list->length--;
}

void moveToHeadPriv(List* list, Node* node) {

    assert ( list->length > 0 );

    if ( list->length == 1 ) {
        return;
    }

    removeNode( list, node );

    addHeadSide( list, node );
}

void moveToHead(List* list, char* key) {
    Node *test = findByKey(list, key);
    if (test != NULL) {
        moveToHeadPriv(list, test);
    }
}

int removeTailSide(List* list) {
    if ( list->length == 0 ) {
        return 1;
    }
    removeNode(list, list->tail);
    return 0;
}

void printNode(Node* node) {
    printf("\n -- node -- BEGIN\n");
    if ( node == NULL ) {
        printf(" --   node is null\n");
    }
    else {
        printf(" --   node:%p\n", node);
        printf(" --   node->key:%s\n", node->key);
        printf(" --   node->value:%s\n", node->value);
        printf(" --   node->len:%d\n", node->len);
        printf(" --   node->prev:%p\n", node->prev);
        printf(" --   node->next:%p\n", node->next);
    }
    printf(" -- node -- END\n");
}

void printList(List* list) {
    printf("list -- BEGIN\n");
    printf("list->length:%d\n", list->length);
    printf("list->head:%p\n", list->head);
    printf("list->tail:%p\n", list->tail);
    Node* current = list->head;
    while ( current != NULL ) {
        printNode(current);
        current = current->next;
    }
    printf("list -- END\n");
}

