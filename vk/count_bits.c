#include <stdio.h>
#include <unistd.h>

int hammingWeight(uint32_t n) {
    int count = 0;
    uint32_t shifting_one = 1;
    for ( int i=0; i<sizeof(uint32_t); i++ ) {
        if ( 0 != ( n & shifting_one ) ) {
            count++;
        }
        shifting_one = shifting_one << 1;
    }
    return count;
}

int main()
{
    for ( int i=0; i<10; i++ ) {
        printf("x%02x - %d\n", i, hammingWeight(i));
    }
    return 0;
}
