#include <stdlib.h>
#include <assert.h>
#include <printf.h>

int compare (const void *a, const void *b) {
    int intA = * ((int*)a);
    int intB = * ((int*)b);
    if ( intA > intB ) {
        return 1;
    }
    if ( intA < intB ) {
        return -1;
    }
    return 0;
}

int find (int* array, int size, int target) {
    int i1 = 0;
    int i2 = size-1;

    if (i1>i2) {
        return -1;
    }

    int center = (i1+i2)/2;

    if ( (target < array[i1]) || ( target > array[i2] ) ) {
        return -1;
    }

    if (array[center] == target) {
        return center;
    }

    if ( target < array[center] ) {
        int ret = find( array+i1, center-i1, target );
        if ( ret == -1 ) {
            return -1;
        }
        return i1 + ret;
    }

    else {  // array[center] > target
        int ret = find( array+(center+1), i2-center, target );
        if ( ret == -1 ) {
            return -1;
        }
        return (center+1) + ret;
    }
}

void assertTest( int x1[], int size, int target, int expectedAt ) {
    qsort ( x1, size, sizeof(x1[0]), compare );
    int foundAt = find( x1, size, target );
    assert( foundAt == expectedAt );
}

int main()
{
    int x1[] = {5,6,1,2,3};
    int len = sizeof(x1) / sizeof(x1[0]);
    
    //
    assertTest( x1, len, 1, 0 );
    assertTest( x1, len, 2, 1 );
    assertTest( x1, len, 3, 2 );
    assertTest( x1, len, 5, 3 );
    assertTest( x1, len, 6, 4 );
    //
    assertTest( x1, len, 4, -1 );
    //
    assertTest( x1, len, -1, -1 );
    assertTest( x1, len, -10, -1 );
    assertTest( x1, len, 10, -1 );
    assertTest( x1, len, 11, -1 );

    printf("done");
    return 0;
}
